#include        <sys/types.h>   /* basic system data types */
#include        <sys/socket.h>  /* basic socket definitions */
#include        <sys/time.h>    /* timeval{} for select() */
#include        <time.h>                /* timespec{} for pselect() */
#include        <netinet/in.h>  /* sockaddr_in{} and other Internet defns */
#include        <arpa/inet.h>   /* inet(3) functions */
#include        <errno.h>
#include        <fcntl.h>               /* for nonblocking */
#include        <netdb.h>
#include        <signal.h>
#include        <stdio.h>
#include        <stdlib.h>
#include        <string.h>
#include        <unistd.h>
#include        "deamon_init.c"
#include	    <syslog.h>
//includy kopiowane z labu 2 (działa z TCP, UDP penie nie)
//define
#define MSG_SIZE 100
#define DATABASE_SIZE 100
#define ALIAS_LEN 10
#define PCK_LEN MSG_SIZE+ALIAS_LEN*2
#define PORT 2020
#define MULTICAST_PORT 3030
#define MULTICAST_GROUP "224.0.1.1"


//struktura pakietu
struct packet
{
    char recipient[ALIAS_LEN];
    char sender[ALIAS_LEN];
    char message[MSG_SIZE];
};
//struktura bazy danych
struct client
{
    char alias[ALIAS_LEN];
    int child_pid;
    int child_pipe;
}database[DATABASE_SIZE] = {0},nullcli;

//deklaracje funkcji, definicje na końcu pliku
void create_packet(struct packet* packet_pointer, char* rec, char* sender,char* msg);
int search_client(char* alias);  //zwraca 1 jeśli istnieje taki klient, 0 jeśli nie
int add_client(char* alias, int pid,int pipe);
int del_client(char* alias);
struct client* get_client(char* alias);
struct client* get_empty_slot();
void create_stream (char* stream, struct packet* packet);

//main
int main(int argc, char const *argv[])
{
    //definicje zmiennych
    int				listenfd, connfd, multicastfd, msglen, childpid, bytes, multicast_addrlen;
	socklen_t			len;
	char				recbuff[PCK_LEN+1],sendbuff[PCK_LEN+1], str[INET_ADDRSTRLEN+1];
	struct sockaddr_in	servaddr, cliaddr, multicast_addr;
    struct packet recpck, sndpck, control_pck;
    char alias[10];
    //definicja pipa do procesu rodzica
    int pipe_to_parent[2];      // communication parent <---- child
    /*
     * pipe_to_parent[0] used in parent process for reading from child processes
     * pipe_to_parent[1] used in child process for writing to parent process
     */

    deamon_init("server", LOG_LOCAL7, getuid());
    openlog("server", LOG_PID | LOG_CONS | LOG_NDELAY, LOG_LOCAL7);
    /*
     * LOG_PID - loguje PID
     * LOG_CONS - loguje do konsoli NIE WIEM CZY TO ZADZIALA...
     * LOG_NDELAY - nie ma opoznien w logowaniu
     * jak tworzyc logi?
     * void syslog(int priority, const char *message, ...);
     * priority  7>>>0: LOG_DEBUG, LOG_INFO, LOG_NOTICE, LOG_WARNING, LOG_ERR, LOG_CRIT, LOG_aLERT, LOG_EMERG ....
     */

    syslog(LOG_NOTICE, "\n\n server turned on by user: %d\n", getuid());
    //printf("server is on\n");

    if (pipe(pipe_to_parent) == -1){
        //fprintf(stderr,"pipe error : %s\n", strerror(errno));
        syslog(LOG_ERR, "pipe error : %m\n");
        exit(-1);
    }
    else
    {
        int flags = fcntl(pipe_to_parent[0],F_GETFL);
        flags = flags | O_NONBLOCK;
        fcntl(pipe_to_parent[0],F_SETFL,flags);
        fcntl(pipe_to_parent[1],F_SETFL,flags);
        syslog(LOG_NOTICE, "pipe to parent successfully created");
    }
    

    int pipe_to_child[2];       // communication parent ----> child
    /*
     * pipe_to_child[0] used in child process for reading from parent process
     * pipe_to_child[1] used in parent process for writing to child process
     */

    //socket to send multicast
    if ( (multicastfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
        syslog(LOG_ERR, "socket() error : %m\n");
        return 1;
    }

    syslog(LOG_NOTICE, "multicast socket successfully created\n");

    bzero((char *)&multicast_addr, sizeof(multicast_addr));
    multicast_addr.sin_family = AF_INET;
    multicast_addr.sin_addr.s_addr = inet_addr(MULTICAST_GROUP);
    multicast_addr.sin_port = htons(MULTICAST_PORT);
    multicast_addrlen = sizeof(multicast_addr);

    //socket
    if ( (listenfd = socket(AF_INET, SOCK_STREAM, 0)) < 0){
                //fprintf(stderr,"socket error : %s\n", strerror(errno));
                syslog(LOG_ERR, "socket() error : %m\n");
                return 1;
        }
    syslog(LOG_NOTICE, "socket successfully created\n");

    //opcje --------------------
    int flags = fcntl(listenfd,F_GETFL);
    flags = flags | O_NONBLOCK;
    fcntl(listenfd,F_SETFL,flags);
    setsockopt(listenfd,SOL_SOCKET,SO_REUSEADDR,&(int){1},sizeof(int));

    //bind 
    servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr   = htonl(INADDR_ANY);
	servaddr.sin_port   = htons(PORT);
    if ( bind( listenfd, (struct sockaddr *) &servaddr, sizeof(servaddr)) < 0){
                //fprintf(stderr,"bind error : %s\n", strerror(errno));
                syslog(LOG_ERR, "bind() error : %m\n");
                return 1;
        }
    syslog(LOG_NOTICE, "bind() success\n");
    //listen
    if ( listen(listenfd, 2) < 0){
                //fprintf(stderr,"listen error : %s\n", strerror(errno));
                syslog(LOG_ERR, "listen() error : %m\n");
                return 1;
        }
    syslog(LOG_NOTICE, "listen() success\n");
    while(1)
    {
        //pętla do akceptowania
        for(;;)
        {
            len = sizeof(cliaddr);
            if ( (connfd = accept(listenfd, (struct sockaddr *) &cliaddr, &len)) < 0){
                syslog(LOG_ERR, "accept() error : %m\n");
                break;
            }
            else
            {
                syslog(LOG_NOTICE, "accept() success\n");
                bzero(&recpck, sizeof(recpck));
                bzero(&recbuff, sizeof(recbuff));
                bzero(&sendbuff, sizeof(sendbuff));
                if (pipe(pipe_to_child) == -1){
                    //fprintf(stderr,"pipe error : %s\n", strerror(errno));
                    syslog(LOG_ERR, "pipe to child error : %m\n");
                    exit(-1);
                }
                else
                {
                    int flags = fcntl(pipe_to_child[0],F_GETFL);
                    flags = flags | O_NONBLOCK;
                    fcntl(pipe_to_child[0],F_SETFL,flags);
                    fcntl(pipe_to_child[1],F_SETFL,flags);
                    syslog(LOG_NOTICE, "pipe to child successfully created\n");
                }
                
                if((childpid = fork())==0){ //proces dziecka - cały
                    close(listenfd);
                    close(pipe_to_child[1]);     // close desc to write to pipe
                    int read_from_parent = pipe_to_child[0];
                    int write_to_parent = pipe_to_parent[1];
                    syslog(LOG_NOTICE, "child successfully created\n");
                    /*
                    * read from parent
                    * ssize_t read(int fd, void *buf, size_t count);
                    * fd ----> desc_to_read_from_parent
                    *************************************************
                    * write to parent
                    * ssize_t write(int fd, const void *buf, size_t count);
                    * fd ----> desc_to_write_to_parent
                    */
                    //klient
                    //odbieranie pakietu - samo odbieranie w podprocesie    
                    while(1)
                    {
                        if((bytes = read(connfd,recbuff,PCK_LEN))>0)
                        {
                            create_packet(&recpck,recbuff,recbuff+ALIAS_LEN,recbuff+2*ALIAS_LEN);
                            strcpy(alias,recbuff);
                            write(write_to_parent,recbuff,PCK_LEN);
                            break;
                        }else if (bytes==-1) syslog(LOG_ERR, "error in read() in child process : %m\n");
                    }         
                    //pętla dziecka
                    for(;;)
                    {
                        bzero(&recpck, sizeof(recpck));
                        bzero(&recbuff, sizeof(recbuff));
                        bzero(&sendbuff, sizeof(sendbuff));
                        
                        if((recv(connfd,recbuff,PCK_LEN,MSG_DONTWAIT))>0)
                        {
                            write(write_to_parent,recbuff,PCK_LEN);
                            //printf("\nmsg:%s!%s!%s\n",recbuff,recbuff+10, recbuff+20);
                        }
                        else
                        {
                            if(errno==88)
                            {
                                close(connfd);
                                create_packet(&control_pck,"server",alias,"close");
                                create_stream(sendbuff,&control_pck);
                                write(write_to_parent,sendbuff,PCK_LEN);
                                sleep(20000);
                            }
                        }
                        
                        if (read(read_from_parent,sendbuff,PCK_LEN)>0)
                        {
                            if(send(connfd,sendbuff,PCK_LEN,0)==-1)
                            {
                                //printf("send err\n");
                                syslog(LOG_ERR, "error in send() in child process : %m\n");
                                close(connfd);
                                create_packet(&control_pck,"server",alias,"close");
                                create_stream(sendbuff,&control_pck);
                                write(write_to_parent,sendbuff,PCK_LEN);
                                sleep(20000);
                            }else{
                                //printf("\nsent resp:%s %s %s\n",sendbuff,sendbuff+10, sendbuff+20);
                            }
                        }
                        
                        
                    }
                }
                else //proces macierzysty
                {
                    close(pipe_to_child[0]);    //close desc to read from pipe
                    int read_from_child = pipe_to_parent[0];
                    int write_to_child = pipe_to_child[1];
                    bzero(&recbuff, sizeof(recbuff));

                    /*
                    * read from child
                    * ssize_t read(int fd, void *buf, size_t count);
                    * fd ----> pipe_to_parent[0]
                    *************************************************
                    * write to child
                    * ssize_t write(int fd, const void *buf, size_t count);
                    * fd ----> database[???].descriptor_to_write_to_child
                    */

                    //funkcja dodająca klienta
                    while(1){
                        if((read(read_from_child,recbuff,PCK_LEN))>0)
                            break;
                        sleep(1);
                    }
                    create_packet(&recpck,recbuff,recbuff+ALIAS_LEN,recbuff+(ALIAS_LEN*2));
                    if(add_client(recpck.sender,childpid,pipe_to_child[1])<1)
                    {
                        //printf("rejected\n");
                        syslog(LOG_NOTICE, "adding client fail, limit of client reached or ...??\n"); //Beniamin
                        create_packet(&control_pck,"0","0","0 name busy or limit of clients reached");
                        create_stream(sendbuff,&control_pck);
                        write(write_to_child,sendbuff,PCK_LEN);
                    }
                    else
                    {
                        //printf("new client: %s!\n", recpck.sender);
                        syslog(LOG_NOTICE, "new client in chat: %s!!!\n", recpck.sender);
                        create_packet(&control_pck,recpck.sender,"server","1 ok");
                        create_stream(sendbuff,&control_pck);
                        write(write_to_child,sendbuff,PCK_LEN);
                        //if(strcmp1(database[0].alias,recpck.sender)==0)
                            //printf("database0: %s\n",database[0].alias);
                    }
                }

            }      
        }

        for(int i=0;i<10;i++) //petla do rutowania
        {
            bzero(&recpck, sizeof(recpck));
            bzero(&recbuff, sizeof(recbuff));
            bzero(&sendbuff, sizeof(sendbuff));
            if(read(pipe_to_parent[0],recbuff,PCK_LEN)>0)
            {
                create_packet(&recpck,recbuff,recbuff+ALIAS_LEN,recbuff+(ALIAS_LEN*2));
                //printf("sender:%s! receipient:%s! message:%s!\n",recpck.sender,recpck.recipient, recpck.message);
                syslog(LOG_NOTICE, "[%s] send message <<< %s >>> to [%s]\n",recpck.sender, recpck.message, recpck.recipient);
                if(strcmp(recpck.recipient,"server") == 0)   //jeśli odbiorcą jest server
                {
                    //jeśli gniazdo dziecka jest zamknięte, usuń proces dziecka
                    if(strcmp(recpck.message,"close")==0)
                    {
                        //printf("child terminate: %s\n", recpck.sender);
                        if(get_client(recpck.sender)->child_pid != 0){
                            syslog(LOG_NOTICE, "child terminate\n");
                            kill(get_client(recpck.sender)->child_pid, SIGKILL);
                            //printf("pid:%d\n",get_client(recpck.sender)->child_pid);
                            del_client(recpck.sender);

                            }
                    }
                    
                }

                if (search_client(recpck.recipient)==1) //przekaż właściwemu dziecku
                {
                    write(get_client(recpck.recipient)->child_pipe,recbuff,PCK_LEN);
                }
                else{
                    if(strcmp(recpck.recipient,"all") == 0) {
                        create_stream(sendbuff, &recpck);
                        if (sendto(multicastfd, sendbuff, PCK_LEN, 0, (struct sockaddr *)&multicast_addr, multicast_addrlen) < 0){
                            syslog(LOG_NOTICE, "multicast sendto() error: %m\n");
                            exit(1);
                        }
                    }else{ //info zwrotne o braku klienta
                        create_packet(&control_pck,recpck.sender,"server","no client in database");
                        syslog(LOG_NOTICE, "%s not in client database\n", recpck.recipient);
                        create_stream(sendbuff, &control_pck);
                        //printf("%s\n",recpck.sender);
                        write(get_client(recpck.sender)->child_pipe, sendbuff, PCK_LEN);
                    }
                }
                
                
            }
            else
                break;
        }
        sleep(1);
    }
    //printf("koniec servera\n");
    syslog(LOG_NOTICE, "END of the server\n\n");
    //close
    close(listenfd);
    close(multicastfd);
    //koniec
    return 0;
}


//definicje funkcji
void create_packet(struct packet* packet_pointer, char* rec, char* sender,char* msg)
{
    strcpy(packet_pointer->recipient,rec);
    strcpy(packet_pointer->sender,sender);
    strcpy(packet_pointer->message,msg);
}
int search_client(char* alias)  //zwraca 1 jeśli istnieje taki klient, 0 jeśli nie
{
    //printf("search:%s\n",alias);
    for (int i=0; i<DATABASE_SIZE; i++)
    {
        if(strcmp(database[i].alias, alias)==0)
            return 1;
    }
    return 0;
}
struct client* get_client(char* alias)
{
    
    for (int i=0; i<DATABASE_SIZE; i++)
    {
        if(strcmp(database[i].alias, alias)==0)
            return &database[i];
    }
    //printf("return nullcli\n");
    syslog(LOG_NOTICE, "return nullcli\n");
    return &nullcli;   
}
struct client* get_empty_slot()
{
    for(int i=0; i < DATABASE_SIZE; i++)
    {
        if(database[i].alias[0] == '\0')
            return &database[i];
    }
    return &nullcli;
}

int add_client(char* alias, int pid, int pipe)
{
    if(search_client(alias)==1)
        return 0;
    else
    {
        struct client* empty = get_empty_slot();
        if (empty == &nullcli)
            return -1;
        strcpy(empty->alias, alias);
        empty->child_pid = pid;
        empty->child_pipe = pipe;
    }
    return 1;
        
}
int del_client(char* alias)
{
    for (int i=0; i<DATABASE_SIZE; i++)
    {
        if(strcmp(database[i].alias, alias)==0)
        {
            database[i].alias[0] = '\0';
            database[i].child_pid = 0;
            database[i].child_pipe = 0;
            return 1;
        }
            
    }
    return 0;
}
void create_stream (char* stream, struct packet* packet)
{
    strcpy(stream, packet->recipient);
    strcpy(stream+ALIAS_LEN, packet->sender);
    strcpy(stream+ALIAS_LEN*2, packet->message);
}
