Rzeczy do zrobienia:
 - Archiektura UDP (nasłuchiwanie na kliencie, wysyłanie na serwerze)
 - Protokół komunikacji, który będzie miał informację o adresacie, nadawcy i wiadomość (wkładamy go w TCP u klienta)
 - Logika Bazy danych serwera (przechowywanie info o klientach i wysyłanie do odpowiednich adresatów) - do zrobienia po zakończeniu protokołu
 - Logika klienta (wysyłanie i odbieranie wiadomości od serwera)
 - Współbierzność serwera - na początku zrobimy 1 klient - 1 serwer, później zaimplementujemy fork();
