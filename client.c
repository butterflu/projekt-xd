#include <sys/types.h>  /* basic system data types */
#include <sys/socket.h> /* basic socket definitions */
#include <netinet/in.h> /* sockaddr_in{} and other Internet defns */
#include <arpa/inet.h>  /* inet(3) functions */
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/select.h>
#include <math.h>

//includy kopiowane z labu 2 (działa z TCP, UDP penie nie)

//define
#define MSG_SIZE 100
#define ALIAS_LEN 10
#define STREAM_SIZE 120
#define MULTICAST_GROUP "224.0.1.1"

char MY_NICK[ALIAS_LEN];

//struktura pakietu
struct packet
{
    char recipient[ALIAS_LEN];
    char sender[ALIAS_LEN];
    char message[MSG_SIZE];
};

//deklaracje funkcji, definicje na dole
void create_packet(struct packet *packet_pointer, char *rec, char *sender, char *msg);
int write_packet(struct packet *pckt);
int read_packet_tcp(struct packet *pckt);
int read_packet_udp(struct packet *pckt);
void send_message(FILE *fp);
void receive_message(FILE *fp);
void sig_chld(int signo);
void sig_pipe(int signo);
void sig_int(int signo);
void clean_exit(int code);
void init(void);
void packet_to_stream(char *stream, struct packet *packet);
void stream_to_packet(struct packet *recv_pckt, char *stream);
void append_dollar_sign(char *s);
void display_packet(struct packet *recv_pckt, int flag);
int create_udp_socket(char const *addr);
int create_tcp_connection(char const *addr);
int get_nick(FILE *fp);
int max(int a, int b);

int udp_socket, tcp_socket; //global variables so we can close socket descriptors in signal handlers and on exit
struct sockaddr_in servaddr_udp;

int main(int argc, char const *argv[]) //przekazujemy adres serwera
{
    //sprawdzamy, czy został przekazany adres ip
    if (argc != 2)
    {
        fprintf(stderr, "ERROR: usage: %s <IPaddress>  \n", argv[0]);
        return 1;
    }

    if (get_nick(stdin) < 0)
    {
        perror("error geting nick");
        clean_exit(1);
    }

    //creating tcp and udp sockets
    if (create_tcp_connection(argv[1]) < 0)
    {
        perror("tcp connection error");
        clean_exit(1);
    }
    if (create_udp_socket(argv[1]) < 0)
    {
        perror("udp socket error");
        clean_exit(1);
    }

    //signal handling
    signal(SIGPIPE, sig_pipe);
    signal(SIGCHLD, sig_chld);
    signal(SIGINT, sig_int);

    init(); //initializing connection - registering to server's database
    //read/write - logika klienta (rozdzielenie czytania i pisania

    int pid = fork(); //dwa procesy - jeden do wysyłania, drugi do odbierania

    if (pid < 0)
    {
        perror("unable to fork process");
        clean_exit(1);
    }

    if (pid == 0) //child process responsible for reading messages
    {
        receive_message(stdout);
    }
    else // parent process responsible for writing
    {
        send_message(stdin);
    }

    //koniec

    //fprintf(stderr, "OK\n");
    fflush(stdout);
    return 0;
}

//funkcje
void create_packet(struct packet *packet_pointer, char *rec, char *sender, char *msg)
{
    strcpy(packet_pointer->recipient, rec);
    strcpy(packet_pointer->sender, sender);
    strcpy(packet_pointer->message, msg);
}

void send_message(FILE *fp)
{
    char msg[MSG_SIZE];
    char rec[ALIAS_LEN];
    char sender[ALIAS_LEN];
    struct packet packet_to_send;

    strcpy(sender, MY_NICK);

    while (1)
    {
        printf("Enter recipient nick: \n");
        if (fgets(rec, ALIAS_LEN - 1, fp) == NULL)
        {
            continue;
        }
        printf("Enter message: \n");
        if (fgets(msg, MSG_SIZE - 1, fp) == NULL)
        {
            printf("something wrong\n");
            continue;
        }
        //deleting new line signs from recipient and msg
        int msg_end = strlen(msg);
        msg[msg_end - 1] = '\0';
        int rec_end = strlen(rec);
        rec[rec_end - 1] = '\0';

        create_packet(&packet_to_send, rec, sender, msg);
        if (write_packet(&packet_to_send) == -1)
        {
            perror("write error");
            clean_exit(1);
        }
    }
}

void receive_message(FILE *fp)
{
    struct packet rcv_pckt;

    while (1)
    {
        fd_set rfd;
        FD_ZERO(&rfd);
        FD_SET(udp_socket, &rfd);
        FD_SET(tcp_socket, &rfd);

        int maxfd = max(udp_socket, tcp_socket);

        if (select(maxfd + 1, &rfd, NULL, NULL, NULL) < 0)
        {
            perror("select error");
            clean_exit(1);
        }
        if (FD_ISSET(udp_socket, &rfd))
        {
            if (read_packet_udp(&rcv_pckt) < 0)
            {
                printf("%s", rcv_pckt.message);
                perror("error reading message");
                clean_exit(1);
            }
            display_packet(&rcv_pckt, 1);
        }
        if (FD_ISSET(tcp_socket, &rfd))
        {
            if (read_packet_tcp(&rcv_pckt) < 0)
            {
                perror("error reading message");
                clean_exit(1);
            }
            display_packet(&rcv_pckt, 0);
        }
    }
}

int write_packet(struct packet *pckt)
{
    char stream[STREAM_SIZE];
    bzero(stream,sizeof(stream));

    packet_to_stream(stream, pckt);

    if (write(tcp_socket, stream, sizeof(stream)) != sizeof(stream))
    {
        return -1;
    }
    return 0;
}

int read_packet_udp(struct packet *pckt)
{
    char stream[STREAM_SIZE + 1];
    socklen_t len = sizeof(servaddr_udp);

    if (recvfrom(udp_socket, stream, STREAM_SIZE, 0, (struct sockaddr *)&servaddr_udp, &len) < 0)
    {
        return -1;
    }
    stream_to_packet(pckt, stream);
    return 0;
}

int read_packet_tcp(struct packet *pckt)
{
    char stream[STREAM_SIZE + 1];
    int n;
    if ((n = read(tcp_socket, stream, sizeof(stream))) < 0)
    {
        perror("read err");
        return -1;
    }

    if (n == 0)
    {
        //printf("%s\n", stream);
        return 0;
    }
    //printf("%d bytes read\n", n);
    stream_to_packet(pckt, stream);
    return n;
}

void sig_chld(int signo)
{
    pid_t pid;
    int stat;

    pid = waitpid(-1, &stat, WNOHANG);
    clean_exit(1);
}

void sig_pipe(int signo)
{
    printf("Server abruptly closed the connection \n");
    clean_exit(1);
}
void sig_int(int signo)
{
    clean_exit(0);
}

void clean_exit(int code)
{
    struct packet exit_packet;
    bzero(&exit_packet, sizeof(exit_packet));
    char rec[ALIAS_LEN] = "server";
    char msg[MSG_SIZE] = "close";
    char sender[ALIAS_LEN];
    strcpy(sender, MY_NICK);

    create_packet(&exit_packet, rec, sender, msg);
    write_packet(&exit_packet);

    close(tcp_socket);
    close(udp_socket);
    exit(code);
}

int get_nick(FILE *fp)
{
    bzero(MY_NICK, sizeof(MY_NICK));
    printf("Welcome to the chat!\n");
    printf("Provide your nick: ");
    if (fgets(MY_NICK, ALIAS_LEN, fp) == NULL)
    {
        return -1;
    }
    int end = strlen(MY_NICK);
    MY_NICK[end - 1] = '\0';
    return 0;
}

void init(void)
{
    char sender[ALIAS_LEN];
    char rec[ALIAS_LEN];
    char msg[MSG_SIZE];
    struct packet init_packet;
    struct packet recv_packet;

    //empty msg to register into server's database of clients
    bzero(msg, sizeof(msg));

    strcpy(sender, MY_NICK);
    strcpy(rec, "serwer"); //recipient set to server
    bzero(&init_packet, sizeof(init_packet));
    bzero(&recv_packet, sizeof(recv_packet));
    create_packet(&init_packet, rec, sender, msg);

    if (write_packet(&init_packet) < 0)
    {
        perror("error sending initialization message");
        clean_exit(1);
    }

    /*
    setting timeout on response from server
    if no response is received from server during 5 seconds client shuts down
    */
    fd_set set;
    struct timeval timeout;
    timeout.tv_sec = 5;
    timeout.tv_usec = 0;
    int rc;

    FD_ZERO(&set);
    FD_SET(tcp_socket, &set);

    rc = select(tcp_socket + 1, &set, NULL, NULL, &timeout);
    if (rc < 0)
    {
        perror("error initialiazing");
        clean_exit(1);
    }
    else if (rc == 0)
    {
        printf("timeout - no response from server\n");
        clean_exit(1);
    }
    if
        FD_ISSET(tcp_socket, &set)
        {
            if (read_packet_tcp(&recv_packet) < 0)
            {
                printf("error receiving response from server\n");
                clean_exit(1);
            }
            if (recv_packet.message[0]=='1')
                printf("init succesful!\n");
            else
            {
                printf("%s\n",recv_packet.message);
                    close(tcp_socket);
                    close(udp_socket);
                    exit(1);
            }
                
        }
}

void packet_to_stream(char *stream, struct packet *packet)
{
    strcpy(stream, packet->recipient);
    strcpy(stream + ALIAS_LEN, packet->sender);
    strcpy(stream + ALIAS_LEN * 2, packet->message);
}

void stream_to_packet(struct packet *recv_pckt, char *stream)
{
    create_packet(recv_pckt, stream, stream + 10, stream + 20);
}

void display_packet(struct packet *recv_pckt, int flag)
{
    // flaga 0 - tcp, 1- udp (multicast)
    if (flag == 1) {
        printf("[UDP MULTICAST] From %s: %s\n", recv_pckt->sender, recv_pckt->message);
    }else{
        printf("[TCP UNICAST] From %s: %s\n", recv_pckt->sender, recv_pckt->message);
    }
}

int create_tcp_connection(char const *addr)
{
    int n, port = 2020;
    struct sockaddr_in servaddr;
    int err;

    if ((tcp_socket = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        return -1;
    }

    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(port);
    if ((err = inet_pton(AF_INET, addr, &servaddr.sin_addr)) <= 0)
    {
        return -1;
    }

    if (connect(tcp_socket, (struct sockaddr *)&servaddr, sizeof(servaddr)) < 0)
    {
        return -1;
    }
    return 0;
}

int create_udp_socket(char const *addr)
{
    int port = 3030;
    struct ip_mreq mreq;
    if ((udp_socket = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
    {
        return -1;
    }
    bzero(&servaddr_udp, sizeof(servaddr_udp));
    servaddr_udp.sin_family = AF_INET;
    servaddr_udp.sin_port = htons(port);
    /*if (inet_pton(AF_INET, addr, &servaddr_udp.sin_addr) <= 0)
    {
        fprintf(stderr,"socket error : %s\n", strerror(errno));
        return -1;
    }*/
    if (bind(udp_socket, (struct sockaddr *)&servaddr_udp, sizeof(servaddr_udp)) < 0)
    {
        fprintf(stderr,"bind error : %s\n", strerror(errno));
        return -1;
    }
    //TODO: subscribe to multicast group
    mreq.imr_multiaddr.s_addr = inet_addr(MULTICAST_GROUP);
    mreq.imr_interface.s_addr = htonl(INADDR_ANY);
    if(setsockopt(udp_socket, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq, sizeof(mreq)) < 0 )
    {
        fprintf(stderr,"setsockopt() error : %s\n", strerror(errno));
        return -1;
    }
    return 0;
}

int max(int a, int b)
{
    if (a >= b)
    {
        return a;
    }
    return b;
}