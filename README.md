# Program typu komunikator tekstowy, używający UDP i TCP

## Opis systemu

Komunikator jest oparty o architekturę typu klient-serwer. Serwer jest współbieżny. Zadaniem serwera jest zarządzanie połączeniami z klientami i przekazywanie wiadomości pomiędzy nimi. Klient łączy się z serwerem przez połączenie TCP, gdzie następuje transmisja wszystkich wiadomości z innymi użytkownikami, a także nasłuchuje na porcie UDP, na ewentualne transmisje multicastowe od serwera. Użytkownik może wysłać wiadomość do wszystkich użytkowników czatu wysyłając wiadomość TCP do serwera, serwer rozsyła tą wiadomość do wszystkich użytkowników przy pomocy UDP. Serwer działa w trybie demona i zapisuje logi do pliku */var/log/local7*. Logi wysylane sa do demona syslog (typ procesu LOG_LOCAL7), trzeba skonfigurowac sysloga (np. /etc/rsyslogd.conf) i utowrzyć plik w odpowiednim miejscu z odpowiednimi uprawnieniami (np. /var/log/local7).

## Instrukcja użytkowania

- po skompilowaniu uruchamiamy program serwera i klienta (podając adres serwera)
- podajemy swój nick, który jest naszym identyfikatorem w systemie
- aby wysłać do kogoś wiadomość musimy znać i podać jego nick
- aby wysłać wiadomość do wszystkich wpisujemy *all* jako odbiorcę

## Autorzy

- Beniamin Paś
- Michał Jaworski
- Cecylia Borek
